/**
 HashDb  is a triple store where the subject, property and object values are passed as Multihash of the actual values
 The actual values are stored externally to the HashDb
 
 As Multihash values are globally unique and immutable they may be sync'd accross multiple instances of the same HashDb
 */
import Foundation
import BitCube

/*
enum GrafDbError: Error {
    case invalidXIndex (Index: UInt32)
    case invalidYIndex (Index: UInt32)
    case invalidZIndex (Index: UInt32)
}
*/

public class HashDb<T: Equatable>: Sequence {
    
    public typealias Element = (T, T, T)
    
    private var nodecount = UInt32(0)

    private var bitcube: BitCube
    
    //The array of all RDF values (subject, predicate, object) used in this hashdb instance
    //The array index for a value is used to map to the underlying bit cube indeces
    private var values = [T]()
    
    public init() throws {
        bitcube = try BitCube(bits: 10)
    }
    
    public var count: Int {
        return bitcube.count
    }

    public func addTriple(_ t: (T, T, T)) {
        setBit((true, t.0, t.1, t.2))
    }
    
    public func removeTriple(_ t: (T, T, T)) {
        setBit((false, t.0, t.1, t.2))
    }
    
    public func setTriple(_ t: (Bool, T, T, T)) {
        setBit(t)
    }
    
    /**
           Return iterator over complete bitcube
     */
    public func makeIterator() -> HashDbIterator {
        return HashDbIterator(bc: bitcube, lookup: values)
    }

    /**
     For each set bit return the X,Y,Z values
     */
    public struct HashDbIterator: IteratorProtocol {
        
        public typealias Element = (T, T, T)
        
        private var i: BitCube.BitCubeIterator
        private var l = [T]()
        
        init(bc: BitCube, lookup: [T]) {
            self.i = bc.makeIterator()
            self.l = lookup
        }
        
        mutating public func next() -> Element? {
            if let n = i.next() {
                let bt = Element(l[Int(n.x)],l[Int(n.y)],l[Int(n.z)])
                return bt
            } else {
                return nil
            }
        }
    }
    
    //--------------------------------------------------
    
    
    private func setBit(_ t: (Bool, T, T, T)) {
        bitcube[indexOf(value: t.1),indexOf(value: t.2),indexOf(value: t.3)] = t.0
    }
    
    private func indexOf(value: T) -> UInt64 {
        if values.firstIndex(of: value) == nil {
            values.append(value)
        }
        
        return UInt64(values.firstIndex(of: value)!)
    }
}

/// Returns true if these arrays of tuple contains the same elements.
public func ==<A: Equatable, B: Equatable, C: Equatable>(lhs: [(A, B, C)], rhs: [(A, B, C)]) -> Bool {
    guard lhs.count == rhs.count else { return false }
    for (idx, lElement) in lhs.enumerated() {
        guard lElement == rhs[idx] else {
            return false
        }
    }
    return true
}




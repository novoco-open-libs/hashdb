import Foundation
import Quick
import Nimble
import HashDb

typealias Multihash = String //Replace with real Multihash impl

final class HashDbTests: QuickSpec {
    override func spec() {
        
        func doTestInitialize() {
            it("can't initialize hashdb") {
                expect {
                    try HashDb<Multihash>()
                }.toNot(throwError())
            }
        }
        
        func doTestAddRemoveTriples(_ values: [(Multihash, Multihash, Multihash)]) {
            it("adreremovetriples") {
                expect {
                    let hdb = try HashDb<Multihash>()
            
                    for v in values {
                        hdb.addTriple((v.0, v.1, v.2))
                        hdb.removeTriple((v.0, v.1, v.2))
                    }
                    return 0
                }.toNot(throwError())
            }
        }
        
        /**
         Test iterator, where inserted values are added in ascending morton order
         */
        func doTestIterate(_ values: [(Multihash, Multihash, Multihash)], _ expected: [(Multihash, Multihash, Multihash)]) {
            var result: [(Multihash, Multihash, Multihash)] = []
            it("iterate \(values)") {
                expect {
                    let hdb = try HashDb<Multihash>()
                
                    for v in values {
                        hdb.addTriple((v.0, v.1, v.2))
                    }
                    
                    for e in hdb {
                        result.append(e)
                    }
                    
                    return 0
                }.toNot(throwError())
                let match = result == expected
                expect(match).to(equal(true))
            }
        }
        
        
        describe("initialization") {
            doTestInitialize()
        }
        
        describe("add/remove") {
            doTestAddRemoveTriples([("","","")])
            doTestAddRemoveTriples([("1","2","3")])
            doTestAddRemoveTriples([("1","2","3"),("1","2","4")])
        }
        
        describe("iterate") {
            doTestIterate([("0", "1", "2")],
                          [("0", "1", "2")])
            doTestIterate([("0", "1", "2"),("1", "5", "9")],
                          [("0", "1", "2"),("1", "5", "9")])
            doTestIterate([("0", "1", "2"),("1", "5", "9"),("0", "2", "5")],
                          [("0", "1", "2"),("0", "2", "5"),("1", "5", "9")])
        }
    }
}

import XCTest

import HashDbTests

var tests = [XCTestCaseEntry]()
tests += HashDbTests.allTests()
XCTMain(tests)
